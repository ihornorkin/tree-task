const router = require('express').Router();
const fs = require('fs');
const dirTree = require("directory-tree");
var rimraf = require("rimraf");

const reply = (res, body, status = 200) => {
  return res.status(status).json(body);
}

router.post('/path', (req, res, next) => {
  try {
    const filesPath = req.body.path;
    if (!req.body.path) {
      throw new SyntaxError('Empty field');
    }
    const fileTree = dirTree(filesPath, { normalizePath: true });
    reply(res, fileTree);
  } catch (err) {
    const error = {
      name: err.name,
      message: err.message
    };
    reply(res, error);
  }

});

router.delete('/path/delete', (req, res) => {
  const { path, rootDirectory } = req.body;

  fs.stat(path, (err, states) => {
    try {
      if (states.isDirectory()) {
        rimraf(path, () => {
          if (err) throw SyntaxError('Opps error');
          const fileTree = dirTree(rootDirectory, { normalizePath: true });
          reply(res, fileTree);
        });
      } else {
        fs.unlink(path, (err) => {
          if (err) throw SyntaxError('Opps error');
          const fileTree = dirTree(rootDirectory, { normalizePath: true });
          reply(res, fileTree);
        });
      }
    } catch (err) {
      const error = {
        name: err.name,
        message: err.message
      };
      reply(res, error);
    }
  });
})

router.post('/path/create', (req, res) => {
  const { path, rootDirectory } = req.body;
  try {
    if (path.includes('.')) {
      const empty = '';
      fs.writeFile(path, empty, 'utf8', (err) => {
        if (err) throw SyntaxError('Opps error');
        const fileTree = dirTree(rootDirectory, { normalizePath: true });
        reply(res, fileTree);
      });
    } else {
      fs.mkdir(path, { recursive: true }, (err) => {
        if (err) throw SyntaxError('Opps error');
        const fileTree = dirTree(rootDirectory, { normalizePath: true });
        reply(res, fileTree);
      });
    }
  } catch (err) {
    const error = {
      name: err.name,
      message: err.message
    };
    reply(res, error);
  }
});

module.exports = router;