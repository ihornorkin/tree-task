var express = require('express');
var api = require('./api');
var bodyParser = require('body-parser');
var port = 3001;

var app = express();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET,DELETE,HEAD,OPTIONS,POST,PUT")
  next();
})
app.use(bodyParser.json());
app.use('/api', api);

app.listen(port, 'localhost', function (err) {
  if (err) {
    console.log(err);
    return
  }

  console.log('Listening at http://localhost:' + port);
})
