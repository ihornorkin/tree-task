import {
    CONTEXT_OPEN,
    CONTEXT_CLOSE
} from '../actionType';

export const openContextMenu = (path, type, coordinates) => ({
    type: CONTEXT_OPEN,
    payload: {
        path,
        type,
        coordinates
    }
});

export const closeContextMenu = () => ({
    type: CONTEXT_CLOSE
});