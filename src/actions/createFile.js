import {
    CREATE_FILE_FAILURE,
    CREATE_FILE_STARTED,
    CREATE_FILE_SUCCESS
} from '../actionType';
import http from '../services/http';

export const createFile = (path) => (dispatch, getState) => {
    dispatch(createFileStarted());

    const rootDirectory = getState().files.path;

    const body = {
        path,
        rootDirectory
    };

    http('/path/create', 'POST', body)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            dispatch(createFileSuccess(data));
        })
        .catch((error) => {
            dispatch(createFileFailure(error));
        })
}

const createFileStarted = () => ({
    type: CREATE_FILE_STARTED
});

const createFileSuccess = (files) => ({
    type: CREATE_FILE_SUCCESS,
    payload: {
        files
    }
});

const createFileFailure = (error) => ({
    type: CREATE_FILE_FAILURE,
    payload: {
        error
    }
});