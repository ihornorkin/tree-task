import { fetchFilesStructure } from './fetchFiles';
import { deleteFile } from './deleteFile';
import { toggleFile } from './toggle';
import { openContextMenu, closeContextMenu } from './contextMenu';
import { createFile } from './createFile';

export const fetchFilesStructureHttp = fetchFilesStructure;
export const deleteFileHttp = deleteFile;
export const toggleAction = toggleFile;
export const openContextMenuAction = openContextMenu;
export const closeContextMenuAction = closeContextMenu;
export const createFileAction = createFile;