import {
    FETCH_FILES_STRUCTURE_STARTED,
    FETCH_FILES_STRUCTURE_SUCCESS,
    FETCH_FILES_STRUCTURE_FAILURE
} from '../actionType';
import http from '../services/http';

export const fetchFilesStructure = (path) => (dispatch) => {
    dispatch(fetchFilesStructureStarted());

    const body = {
        path
    };

    http('/path', 'POST', body)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            dispatch(fetchFilesStructureSuccess(data));
        })
        .catch((error) => {
            dispatch(fetchFilesStructureFailure(error));
        })
}

const fetchFilesStructureStarted = () => ({
    type: FETCH_FILES_STRUCTURE_STARTED
});

const fetchFilesStructureSuccess = (files) => ({
    type: FETCH_FILES_STRUCTURE_SUCCESS,
    payload: {
        files
    }
});

const fetchFilesStructureFailure = (error) => ({
    type: FETCH_FILES_STRUCTURE_FAILURE,
    payload: {
        error
    }
});