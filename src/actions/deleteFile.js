import {
    DELETE_FILE_STARTED,
    DELETE_FILE_SUCCESS,
    DELETE_FILE_FAILURE,
} from '../actionType';
import http from '../services/http';

export const deleteFile = (path) => (dispatch, getState) => {
    dispatch(deleteFileStarted());

    const rootDirectory = getState().files.path;

    const body = {
        path,
        rootDirectory
    };

    http('/path/delete', 'DELETE', body)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            dispatch(deleteFileSuccess(data));
        })
        .catch((error) => {
            console.log(error);
            dispatch(deleteFileFailure(error));
        })
}

const deleteFileStarted = () => ({
    type: DELETE_FILE_STARTED
});

const deleteFileSuccess = (files) => ({
    type: DELETE_FILE_SUCCESS,
    payload: {
        files
    }
});

const deleteFileFailure = (error) => ({
    type: DELETE_FILE_FAILURE,
    payload: {
        error
    }
});