import {
    TOGGLE_OPEN_CLOSE
} from '../actionType';

export const toggleFile = (files) => {
    return {
        type: TOGGLE_OPEN_CLOSE,
        payload: {
            files
        }
    }
};