export const hasChildren = (folder) => {
    if (folder.type === 'directory' && folder.children.length) {
        return true;
    } else {
        return false;
    }
}

export const toogleFile = (fileTree, fileTarget) => {
    if (fileTree.path === fileTarget) {
        const isOpen = !fileTree.isOpen;
        return {
            ...fileTree,
            isOpen
        }
    };
    if (hasChildren(fileTree)) {
        const newChildrenProperty = (fileTree.children.map((file) => {
            if (hasChildren(file)) {
                toogleFile(file, fileTarget);
            }
            if (file.path === fileTarget) {
                file.isOpen = !file.isOpen;
            }
            return file;
        }));
        fileTree.children = [ ...newChildrenProperty ];
        return fileTree;
    } else {
        return fileTree;
    };
};

export const modifyObject = (fileTree, isOpen) => {
    fileTree.isOpen = isOpen;
    if (hasChildren(fileTree)) {
        fileTree.children.forEach((file) => {
            if (hasChildren(file)) {
                modifyObject(file);
                file.isOpen = isOpen;
                return file;
            }
        });
        return fileTree;
    } else {
        return fileTree;
    }
};