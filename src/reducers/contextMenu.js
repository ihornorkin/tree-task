import {
    CONTEXT_CLOSE,
    CONTEXT_OPEN
} from '../actionType';

const initialState = {
    isOpen: false,
    path: null,
    type: null,
    coordinates: {}
}

const contextMenu = (state = initialState, action) => {
    switch (action.type) {
        case CONTEXT_OPEN:
            const newWindow = { ...action.payload, isOpen: true };
            return {
                ...state,
                ...newWindow
            }
        case CONTEXT_CLOSE:
            return {
                ...state,
                isOpen: false
            }
        default:
            return state
    }
}

export default contextMenu;