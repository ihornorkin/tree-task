import { combineReducers } from 'redux';
import files from './files';
import contextMenu from './contextMenu';

export default combineReducers({
    files,
    contextMenu
})