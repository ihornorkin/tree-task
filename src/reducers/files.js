import {
    FETCH_FILES_STRUCTURE_STARTED,
    FETCH_FILES_STRUCTURE_SUCCESS,
    FETCH_FILES_STRUCTURE_FAILURE,
    TOGGLE_OPEN_CLOSE,
    DELETE_FILE_FAILURE,
    DELETE_FILE_SUCCESS,
    DELETE_FILE_STARTED,
    CREATE_FILE_FAILURE,
    CREATE_FILE_SUCCESS,
    CREATE_FILE_STARTED
} from '../actionType';
import * as merge from 'deepmerge';
import { modifyObject, toogleFile } from '../utils/';

const initialState = {}

const files = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_FILES_STRUCTURE_STARTED:
        case CREATE_FILE_STARTED:
            return {
                ...state
            }
        case FETCH_FILES_STRUCTURE_SUCCESS:
            const newObject = modifyObject(action.payload.files, false);
            return {
                ...state,
                ...newObject
            };
        case FETCH_FILES_STRUCTURE_FAILURE:
        case DELETE_FILE_FAILURE:
        case CREATE_FILE_FAILURE:
            return {
                ...state,
                error: action.payload.error
            }
        case TOGGLE_OPEN_CLOSE:
            const result = toogleFile(state, action.payload.files);
            return {
                ...state,
                ...result
            }
        case DELETE_FILE_STARTED:
            return {
                ...state
            }
        case DELETE_FILE_SUCCESS:
        case CREATE_FILE_SUCCESS:
            const overwriteMerge = (destinationArray, sourceArray) => {
                const mergeArr = sourceArray.map((source) => {
                    destinationArray.forEach((destination) => {
                        if (destination.path === source.path) {
                            if (destination.isOpen) {
                                const conditional = destination.isOpen;
                                source.isOpen = conditional;
                            }
                        }
                    });
                    return source;
                });
                return mergeArr;
            }
            const mergeState = merge(state, action.payload.files, { arrayMerge: overwriteMerge });
            return {
                ...state,
                ...mergeState
            };    
        default:
            return state
    }
}

export default files;