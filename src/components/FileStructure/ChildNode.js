import React from 'react';
import emptyFolder from '../../images/empty-folder.png';
import fileIcon from '../../images/file.png';

const ChildNode = ({ file, handleContextMenu }) => {
    return (
        <div className="child file-item"
            key={file.name}
            onContextMenu={(e) => handleContextMenu(e, file.path, file.type)} >
            {file.type === 'directory' ? <img src={emptyFolder} alt="empty folder" /> : <img src={fileIcon} alt="file" />} 
            {file.name}
        </div>
    )
};

export default ChildNode;