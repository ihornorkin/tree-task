import React from 'react';
import closeFolder from '../../images/close-folder.png';
import openFolder from '../../images/open-folder.png';

const ParentNode = ({ file, children, toogleFolder, handleContextMenu }) => {
    return (
        <div className="nodeBranch">
            <div className="parentNode file-item"
                onClick={() => toogleFolder(file.path)}
                onContextMenu={(e) => handleContextMenu(e, file.path, file.type)}>
                {file.isOpen ? <img src={openFolder} alt="folder" /> : <img src={closeFolder} alt="folder" />}
                {file.name}
            </div>
            <div className="childNode">
                {file.isOpen ? children : null}
            </div>
        </div>
    )
}

export default ParentNode;