import React, { Component } from 'react';
import { connect } from 'react-redux';
import ChildNode from './ChildNode';
import ParentNode from './ParentNode';
import { hasChildren } from '../../utils';
import { deleteFileHttp, toggleAction, openContextMenuAction, closeContextMenuAction } from '../../actions';
import './FileStructure.scss';
import ContextMenu from '../ContextMenu';

class FileStructure extends Component {
    renderTree = (files) => {
        const { deleteFile, toggleFile } = this.props;
        if (files && files.name) {
            let children = null;
            if (files.children) {
                children = files.children.map((item) => {
                    if (hasChildren(item)) {
                        return (
                            this.renderTree(item)
                        )
                    } else {
                        return (
                            <ChildNode file={item}
                                key={item.path}
                                deleteFile={deleteFile}
                                handleClick={this.handleClick}
                                handleContextMenu={this.handleContextMenu}
                            />
                        )
                    }
                });
            }
            return (
                <ParentNode key={files.path}
                    file={files}
                    deleteFile={deleteFile}
                    toogleFolder={toggleFile}
                    handleContextMenu={this.handleContextMenu}
                >
                    {children}
                </ParentNode>
            )
        }
    };

    handleClick = () => {
        const { closeContextMenu, contextMenu } = this.props;
        if (contextMenu.isOpen) {
            closeContextMenu();
        }
    };

    handleContextMenu = (e, path, type) => {
        e.preventDefault();
        const { openContextMenu } = this.props;
        const coordinates = {
            x: e.pageX,
            y: e.pageY
        }
        openContextMenu(path, type, coordinates);
    }

    render() {
        const { files, contextMenu } = this.props;
        return (
            <div className="fileStructure" onClick={this.handleClick} >
                <div className="fileStructure-container">
                    {this.renderTree(files)}
                    { contextMenu.isOpen ? <ContextMenu /> : null }
                </div>
            </div>
        )
    }
};

const mapStateToProps = (state) => ({
    files: state.files,
    contextMenu: state.contextMenu
});

const mapDispatchToProps = (dispatch) => {
    return {
        deleteFile: (path) => {
            dispatch(deleteFileHttp(path))
        },
        toggleFile: (path) => {
            dispatch(toggleAction(path))
        },
        openContextMenu: (path, type, coordinates) => {
            dispatch(openContextMenuAction(path, type, coordinates))
        },
        closeContextMenu: () => {
            dispatch(closeContextMenuAction());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FileStructure);