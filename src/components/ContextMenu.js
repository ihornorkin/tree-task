import React, { Component } from 'react';
import { connect } from 'react-redux';
import './ContextMenu.scss';
import { deleteFileHttp, createFileAction, closeContextMenuAction } from '../actions';

class ContextMenu extends Component {
    state = {
        isCreate: false,
        fileName: null,
        isValid: true
    }

    handleChange = (e) => {
        const value = e.target.value;
        this.setState({
            fileName: value
        });
    }

    handleCreate = () => {
        const { path } = this.props.contextMenu;
        const { fileName } = this.state;
        this.setState({
            isValid: true
        });
        if (fileName) {
            const value = `${path}/${fileName}`;
            this.props.createFile(value);
            this.props.closeContextMenu();
        } else {
            this.setState({
                isValid: false
            });
        }
    }

    hadnleDelete = () => {
        const { contextMenu, deleteFile, closeContextMenu } = this.props;
        deleteFile(contextMenu.path);
        closeContextMenu();
    }

    handleClick = (e) => {
        e.stopPropagation();
    }

    render() {
        const { contextMenu } = this.props;
        const { isValid } = this.state;
        const style = {
            top: contextMenu.coordinates.y,
            left: contextMenu.coordinates.x
        };
        const newFile = <li className="new-file">
            <input type="text"
                placeholder="New file"
                onChange={this.handleChange} />
            <button onClick={this.handleCreate}>Create new</button>
            {isValid ? null : <p className="tooltip tooltip-red">The field is require</p>}
        </li>;
        return (
            <div className="context-window" onClick={this.handleClick} style={style} >
                <ul>
                    {contextMenu.type === 'directory' ? newFile : null}
                    <li>
                        <button onClick={this.hadnleDelete}>Delete</button>
                    </li>
                </ul>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    contextMenu: state.contextMenu
});

const mapDispatchToProps = (dispatch) => {
    return {
        deleteFile: (path) => {
            dispatch(deleteFileHttp(path))
        },
        createFile: (newFile) => {
            dispatch(createFileAction(newFile))
        },
        closeContextMenu: () => {
            dispatch(closeContextMenuAction());
        }
    }
};


export default connect(mapStateToProps, mapDispatchToProps)(ContextMenu);