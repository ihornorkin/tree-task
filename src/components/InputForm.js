import React, { Component } from 'react';
import { fetchFilesStructureHttp, closeContextMenuAction } from '../actions';
import { connect } from 'react-redux';
import './InputForm.scss';

class InputForm extends Component {
    state = {
        path: null,
        isValid: true
    }

    handleChange = (e) => {
        const value = e.target.value;
        this.setState({
            path: value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        const { fetchFilesStructureHttp, closeContextWindow } = this.props;
        const { path } = this.state;
        this.setState({
            isValid: true
        });
        if (path) {
            fetchFilesStructureHttp(path);
            closeContextWindow();
        } else {
            this.setState({
                isValid: false
            });
        }
    }

    render() {
        const { isValid } = this.state;
        return (
            <div className="from-wrapper">
                <form onSubmit={this.handleSubmit} className="new-files">
                    <input type="text" name="directory" onChange={this.handleChange}></input>
                    <button type="submit">Submit</button>
                </form>
                <p className="tooltip">Expect standart unix path. Example '/path/example' or for window 'C:/path/example'</p>
                {isValid ? null : <p className="tooltip tooltip-red">The field is require</p>}
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchFilesStructureHttp: (path) => {
            dispatch(fetchFilesStructureHttp(path))
        },
        closeContextWindow: () => {
            dispatch(closeContextMenuAction());
        }
    }
}

export default connect(null, mapDispatchToProps)(InputForm);