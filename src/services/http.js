const URL = 'http://localhost:3001/api';

const http = (endpoint, method, body) => {
  const header = new Headers({
    'Content-Type': 'application/json'
  });
  const options = {
    method,
    headers: header,
    body: JSON.stringify(body)
  }
  return fetch(`${URL}${endpoint}`, options);
}

export default http;