import React, { Component } from 'react';
import InputForm from './components/InputForm';
import FileStructure from './components/FileStructure/FileStructure';
import "./App.scss";

class App extends Component {
  state = {
    path: null
  }

  render() {
    return (
      <div className="app">
        <InputForm />
        <FileStructure />
      </div>
    );
  }
}

export default App;
